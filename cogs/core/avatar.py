"""Contains a cog that fetches user avatars."""

import discord
from discord.ext import commands
from kitsuchan import helpers

VALID_SIZES = [16, 32, 64, 128, 256, 512, 1024]


class Avatar(commands.Cog):
    """Avatar commands."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def avatar(self, ctx, *, user: str = None):
        """Display a user's avatar.

        * user - A text string that the bot will attempt to use to look up a user."""
        if not user:
            user = ctx.author
        elif ctx.guild:
            user = await helpers.member_by_substring(ctx, user)
        else:
            raise commands.NoPrivateMessage("You cannot specify a user for this command in private messages.")
        url = user.avatar_url_as(static_format="png")
        embed = discord.Embed(description=f"Avatar for {user.mention}")
        embed.set_image(url=url)
        await ctx.send(embed=embed)

    @commands.command(aliases=["gicon", "servericon", "sicon"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def guildicon(self, ctx):
        """Display the icon of the current guild."""
        await ctx.send(ctx.guild.icon_url)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Avatar())
