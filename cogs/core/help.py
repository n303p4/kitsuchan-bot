"""A butchered help command."""

import discord
from discord.ext import commands


def embed_from_dict(dictionary):
    """Convert a specially crafted dict into an embed."""
    embed = discord.Embed()
    for key, value in sorted(dictionary.items(), key=lambda x: x[0]):
        if isinstance(value, list):
            value = sorted(value)
            value = ", ".join(value)
        embed.add_field(name=key, value=value, inline=False)
    return embed


class AutofoxHelpCommand(commands.DefaultHelpCommand):
    """Autofox help command."""

    async def send_bot_help(self, mapping):
        command_categories = {}
        bot_commands = await self.filter_commands(self.context.bot.commands)

        for command in bot_commands:
            module_name = command.callback.__module__.split(".")
            if len(module_name) == 1:
                command_category = module_name[0]
            else:
                command_category = module_name[0] if module_name[1] == "cogs" else module_name[1]
            command_category = command_category.replace("_", " ").title()
            if command_category not in command_categories:
                command_categories[command_category] = []
            command_categories[command_category].append(command.name)
            if len(command.aliases) > 10:
                command_categories[command.name.capitalize()] = command.aliases

        embed = embed_from_dict(command_categories)
        embed.description = (f"Use `{self.clean_prefix}help <command_name>` "
                             "for more details on a command.")

        destination = self.get_destination()
        await destination.send(embed=embed)


    async def send_command_help(self, command):
        embed = discord.Embed()

        signature = self.get_command_signature(command)
        embed.title = signature

        if command.help:
            embed.description = command.help

        destination = self.get_destination()
        await destination.send(embed=embed)


def setup(bot):
    """Set up the extension."""
    bot.help_command = AutofoxHelpCommand()
