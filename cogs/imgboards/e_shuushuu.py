"""e-shuushuu module."""

import secrets
from urllib.parse import urlencode, urljoin

from bs4 import BeautifulSoup
import discord
from discord.ext import commands
from discord.ext.commands import UserInputError

from kitsuchan.exceptions import WebAPIUnreachable, WebAPINoResultsFound


BASE_URLS = {"e-shuushuu": {"image_search": "https://e-shuushuu.net/search/process/",
                            "tag_search": "https://e-shuushuu.net/httpreq.php?{0}",
                            "image_post": "https://e-shuushuu.net"}}
SEARCH_HEADERS = {"Content-Type": "application/x-www-form-urlencoded"}


async def _eshuushuu_tag_search(session, base_url: str, search_text: str):
    query_params = urlencode({
        "mode": "tag_search",
        "tags": search_text.strip("\"'"),
        "type": 1
    })
    url = base_url.format(query_params)
    async with session.post(url) as response:
        if response.status < 400:
            return await response.text()
        else:
            raise WebAPIUnreachable(service="e-shuushuu")


async def _eshuushuu(session, base_url: str, search_headers: dict,
                     tags: list = None, characters: list = None):
    """Generic helper command that can handle e-shuushuu

    * site - The site to check.
    * tags - A list of tags to be used in the search criteria.
    """
    if not tags:
        tags = []
    tags = [f'"{tag}"' for tag in tags]

    if not characters:
        characters = []
    characters = [f'"{character}"' for character in characters]

    if not tags and not characters:
        raise UserInputError("You must provide at least one tag.")

    formdata = urlencode({
        "tags": "+".join(tags),
        "char": "+".join(characters)
    })
    async with session.post(base_url, data=formdata, headers=search_headers) as response:
        if response.status < 400:
            xml = await response.text()
            soup = BeautifulSoup(xml, features="lxml")
            posts = soup.find_all("a", href=True, class_="thumb_image")
            if not posts:
                raise WebAPINoResultsFound(message=(
                    "No results found. Make sure you're enclosing multi-word tags in quotes, "
                    "such as \"pointy ears\". You can use the command `est` to find valid tags."
                ))
            post = secrets.choice(posts)["href"]
            return post
        else:
            raise WebAPIUnreachable(service="e-shuushuu")


class EShuushuu(commands.Cog):
    """e-shuushuu lookup commands."""

    @commands.command(aliases=["est"])
    @commands.cooldown(6, 12)
    async def eshuushuutag(self, ctx, *, search_text):
        """Search e-shuushuu for valid tags.

        This command does **not** currently accept character names.
        """
        result = await _eshuushuu_tag_search(
            ctx.bot.session,
            BASE_URLS["e-shuushuu"]["tag_search"],
            search_text
        )
        if result.strip("\n"):
            embed = discord.Embed(title="Matching tags", description=result)
            await ctx.send(embed=embed)
        else:
            await ctx.send("No matching tags were found.")

    @commands.command(aliases=["senko-san"])
    @commands.cooldown(6, 12)
    async def senko(self, ctx, *tags):
        """Fetch a random Senko. Optional tags accepted."""
        result = await _eshuushuu(
            ctx.bot.session,
            BASE_URLS["e-shuushuu"]["image_search"],
            SEARCH_HEADERS,
            tags,
            ["Senko"]
        )
        image_url = urljoin(BASE_URLS["e-shuushuu"]["image_post"], result.lstrip("/"))
        await ctx.send(image_url)

    @commands.command(aliases=["es", "shuushuu"])
    @commands.cooldown(6, 12)
    async def eshuushuu(self, ctx, *tags):
        """Fetch a random image from e-shuushuu. You must provide at least one tag.

        * tags - A list of tags to be used in the search criteria.

        **Usage notes**
        * Valid tags can be found using the `est` command.
        * This command does **not** currently accept character names.
        * Multi-word tags should be surrounded with quotes, e.g. \"kitsune mimi\".
        """
        result = await _eshuushuu(
            ctx.bot.session,
            BASE_URLS["e-shuushuu"]["image_search"],
            SEARCH_HEADERS,
            tags
        )
        image_url = urljoin(BASE_URLS["e-shuushuu"]["image_post"], result.lstrip("/"))
        await ctx.send(image_url)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(EShuushuu())
