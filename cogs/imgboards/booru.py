"""Imageboard lookup commands."""

# pylint: disable=C0103

import json
import re
import urllib.parse
import secrets

from bs4 import BeautifulSoup
import discord
from discord.ext import commands

from kitsuchan.exceptions import WebAPIUnreachable, WebAPINoResultsFound

BASE_URLS = {"safebooru": {"image_search": "https://safebooru.org/index.php?{0}",
                           "tag_search": "https://safebooru.org/autocomplete.php?{0}",
                           "image_post": "https://safebooru.org/index.php?page=post&s=view&id={0}"}}
MAX_LENGTH_TAGS = 2048
TAGS_BLOCKLIST = ["loli", "shota", "lolicon", "shotacon", "scat"]


async def _booru_tag_search(session, base_url: str, search_text: str = "", blocklist: list = None):
    """Generic helper that can find tags on booru-type sites."""
    if not blocklist:
        blocklist = TAGS_BLOCKLIST
    query_params = urllib.parse.urlencode({"q": search_text.replace(" ", "_")})
    url = base_url.format(query_params)
    async with session.get(url) as response:
        if response.status == 200:
            results = json.loads(await response.text())
            if not results:
                raise WebAPINoResultsFound(message="No tags found.")
            return [result["label"] for result in results if result["value"] not in blocklist]
    raise WebAPIUnreachable(service="Safebooru")


async def _booru(session, base_url_api: str, tags: list = None, blocklist: list = None):
    """Generic helper command that can retrieve image posts from most 'booru-type sites."""
    if not tags:
        tags = []
    tags = [tag.replace(" ", "_") for tag in tags]
    if not blocklist:
        blocklist = TAGS_BLOCKLIST
    for tag in blocklist:
        tags.append(f"-{tag}")
    query_params = urllib.parse.urlencode({
        "page": "dapi",
        "s": "post",
        "q": "index",
        "tags": " ".join(tags)
    })
    url = base_url_api.format(query_params)
    async with session.get(url) as response:
        if response.status == 200:
            xml = await response.text()
            soup = BeautifulSoup(xml, features="lxml")
            posts = soup.find_all("post")
            if not posts:
                raise WebAPINoResultsFound(message=(
                    "No results found. Make sure you're using standard booru-type tags, such as "
                    "fox_ears or red_hair. You can use the command `sbt` to find valid tags."
                ))
            post = secrets.choice(posts)
            return post
    raise WebAPIUnreachable(service="Safebooru")


def _process_post(post, base_url_post: str, max_length_tags: int = MAX_LENGTH_TAGS):
    """Make an embed that renders a 'booru post."""
    if re.match(r"http(s?):\/\/.+", post['sample_url']):
        sample_url = post['sample_url']
    else:
        sample_url = f"https:{post['sample_url']}"
    if re.match(r"http(s?):\/\/.+", post['file_url']):
        file_url = post['file_url']
    else:
        file_url = f"https:{post['file_url']}"
    post_url = base_url_post.format(post["id"])
    embed = discord.Embed(title=post["id"])
    embed.url = post_url
    embed.description = f"[Full-size image]({file_url})"
    embed.set_image(url=sample_url)
    embed.set_footer(text=post["tags"][:max_length_tags])
    return embed


class Booru(commands.Cog):
    """Imageboard lookup commands."""

    @commands.command(aliases=["sbt"])
    @commands.cooldown(6, 12)
    async def safeboorutag(self, ctx, *, search_text):
        """Search Safebooru for valid tags.

        * search_text - Text to search for.
        """
        result = await _booru_tag_search(ctx.bot.session, BASE_URLS["safebooru"]["tag_search"], search_text)
        formatted_results = "\n".join([r.replace("_", "\\_") for r in result])
        embed = discord.Embed(title="Matching tags", description=formatted_results)
        await ctx.send(embed=embed)

    @commands.command(aliases=["sbooru", "sb"])
    @commands.cooldown(6, 12)
    async def safebooru(self, ctx, *tags):
        """Fetch a random image from Safebooru. Tags accepted.

        * tags - A list of tags to be used in the search criteria.

        **Usage notes**
        * Valid tags can be found using the `sbt` command.
        * Underscores do not have to be used. But multi-word tags should be surrounded with quotes, e.g. \"fox ears\".

        See http://safebooru.org/index.php?page=help&topic=cheatsheet for more details.
        """
        result = await _booru(ctx.bot.session, BASE_URLS["safebooru"]["image_search"], tags)
        embed = _process_post(result, BASE_URLS["safebooru"]["image_post"])
        await ctx.send(embed=embed)

    @commands.command(aliases=["meido"])
    @commands.cooldown(6, 12)
    async def maid(self, ctx, *tags):
        """Find a random maid. Optional tags."""
        result = await _booru(ctx.bot.session, BASE_URLS["safebooru"]["image_search"], ["maid"] + tags)
        embed = _process_post(result, BASE_URLS["safebooru"]["image_post"])
        await ctx.send(embed=embed)

    @commands.command(aliases=["animememe"])
    @commands.cooldown(6, 12)
    async def animeme(self, ctx, *tags):
        """Find a random anime meme. Optional tags."""
        result = await _booru(ctx.bot.session, BASE_URLS["safebooru"]["image_search"], ["meme"] + tags)
        embed = _process_post(result, BASE_URLS["safebooru"]["image_post"])
        await ctx.send(embed=embed)

    @commands.command(name=":<")
    @commands.cooldown(6, 12)
    async def colonlessthan(self, ctx, *tags):
        """:<"""
        result = await _booru(ctx.bot.session, BASE_URLS["safebooru"]["image_search"], [":<"] + tags)
        embed = _process_post(result, BASE_URLS["safebooru"]["image_post"])
        await ctx.send(embed=embed)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Booru())
