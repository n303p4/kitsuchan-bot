"""Bot utility commands."""

import os

import discord
from discord.ext import commands

import bot_info


class Utilities(commands.Cog):
    """Utility commands for the bot owner.

    These commands will not show up in the help unless you're the owner.
    """

    @commands.command()
    @commands.is_owner()
    async def rename(self, ctx, *, username):
        """Change the bot's username. Bot owner only.

        * username - The new username to assign the bot.
        """
        await ctx.bot.user.edit(username=username)
        await ctx.send(f"Bot username changed.")

    @commands.command()
    @commands.is_owner()
    async def setavatar(self, ctx, *, filename):
        """Change the bot's avatar. Bot owner only.

        * filename - The filename of the avatar to assign the bot.
        """
        if os.path.isfile(filename):
            with open(filename, "rb") as fileobject:
                avatar = fileobject.read()
            await ctx.bot.user.edit(avatar=avatar)
            await ctx.send("Bot avatar changed.")
        else:
            raise commands.UserInputError("Not a valid filename.")

    @commands.command(aliases=["status"])
    @commands.is_owner()
    async def setstatus(self, ctx, *, status=None):
        """Change the bot's playing status. Bot owner only.

        * status - The text to display in the playing status.
        """
        if status:
            activity = discord.Game(name=status)
            await ctx.bot.change_presence(activity=activity)
            await ctx.send("Bot playing status set.")
        else:
            await ctx.bot.change_presence(activity=None)
            await ctx.send("Bot playing status cleared.")

    @commands.command(aliases=["clean"])
    @commands.is_owner()
    async def censor(self, ctx, times: int = 1):
        """Delete the bot's previous message(s). Bot owner only.

        * times - Number of message to delete. Defaults to 1.
        """
        if times < 1:
            return commands.UserInputError("Can't delete less than 1 message.")
        times_executed = 0
        async for message in ctx.channel.history():
            if times_executed == times:
                break
            if message.author.id == ctx.bot.user.id:
                await message.delete()
                times_executed += 1

    @commands.command(aliases=["say"])
    @commands.is_owner()
    async def echo(self, ctx, *, text=""):
        """Repeat the user's text back at them. Bot owner only.

        * text - A string to be echoed back.
        """
        if not text:
            text = "Echo?"
        # This mostly prevents the bot from triggering other bots.
        text = "\u200B" + text
        await ctx.send(text)

    @commands.command(aliases=["vpatch"])
    @commands.is_owner()
    async def versionpatch(self, ctx, major: int, minor: int, patch: int, *, codename):
        """Update the bot version."""
        version_info = (major, minor, patch, codename)
        bot_info.version_info = version_info
        bot_info.version_number = f"{version_info[0]}.{version_info[1]}.{version_info[2]}"
        bot_info.version = "{0}.{1}.{2} \"{3}\"".format(*version_info)
        await ctx.send("Version number patched.")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Utilities())
