"""Contains a cog that fetches colors."""

# pylint: disable=C0103

import colorsys
import secrets

import discord
from discord.ext import commands
import webcolors

BASE_URL_COLOR_API = "https://www.colourlovers.com/img/{0}/{1}/{2}/"
BASE_URL_TINEYE_MULTICOLR = "https://labs.tineye.com/multicolr/#colors={0};weights=100"
BASE_URL_COLOR_HEX = "https://www.color-hex.com/color/{0}"
BASE_URL_ENCYCOLORPEDIA = "https://encycolorpedia.com/{0}"


def rgb_to_hsv(red, green, blue):
    """Convert an RGB tuple to an HSV tuple."""
    hue, saturation, value = colorsys.rgb_to_hsv(red/255, green/255, blue/255)
    return int(hue*360), int(saturation*100), int(value*100)


def rgb_to_hls(red, green, blue):
    """Convert an RGB tuple to an HLS tuple."""
    hue, lightness, saturation = colorsys.rgb_to_hls(red/255, green/255, blue/255)
    return int(hue*360), int(lightness*100), int(saturation*100)


def color_from_string(color):
    """Given a string, get color info."""
    try:
        color = webcolors.name_to_hex(color)
    except (ValueError, AttributeError):
        pass
    try:
        if color:
            color = color.lstrip("#")  # Remove the pound sign.
            color = int(f"0x{color}", 16)
        else:
            color = secrets.randbelow(16777216)
        color = discord.Color(color)
    except ValueError:
        raise commands.UserInputError(("Not a valid color. "
                                       "Color must either be A) in hex format (e.g. `808080`)"
                                       " and between `FFFFFF` and `000000`, or B) A named CSS"
                                       " color (e.g. `red` or `lightsteelblue`)."))
    return color


class Color(commands.Cog):
    """Color command."""

    @commands.command(aliases=["simplecolour", "scolor", "scolour"])
    @commands.cooldown(6, 12)
    async def simplecolor(self, ctx, *, color: str = None):
        """Display a color, without detailed info. Accepts CSS color names and hex input.

        * color - Either a CSS color or hex input.
        """
        color = color_from_string(color)
        color_hex_value = f"{color}"

        embed = discord.Embed()
        embed.colour = color
        image_url = BASE_URL_COLOR_API.format(color_hex_value.lstrip("#"), 88, 88)
        embed.set_image(url=image_url)
        embed.description = color_hex_value

        await ctx.send(embed=embed)

    @commands.command(aliases=["colour"])
    @commands.cooldown(6, 12)
    async def color(self, ctx, *, color: str = None):
        """Display a color, with detailed info. Accepts CSS color names and hex input.

        * color - Either a CSS color or hex input.
        """
        color = color_from_string(color)
        color_hex_value = f"{color}".lstrip("#")

        embed = discord.Embed()
        embed.colour = color
        image_url = BASE_URL_COLOR_API.format(color_hex_value, 88, 88)
        embed.set_thumbnail(url=image_url)
        color_as_rgb = color.to_rgb()
        color_as_rgba = color_as_rgb + (1.0,)
        embed.add_field(name="RGB", value=f"rgb{color_as_rgb}")
        embed.add_field(name="RGBA", value=f"rgba{color_as_rgba}")
        embed.add_field(name="HSV*", value=f"{rgb_to_hsv(*color_as_rgb)}")
        embed.add_field(name="HLS*", value=f"{rgb_to_hls(*color_as_rgb)}")
        embed.add_field(name="Hex code", value=f"#{color_hex_value}")
        embed.add_field(name="Images",
                        value=BASE_URL_TINEYE_MULTICOLR.format(color_hex_value))

        information_links = (f"{BASE_URL_COLOR_HEX.format(color_hex_value)}\n"
                             f"{BASE_URL_ENCYCOLORPEDIA.format(color_hex_value)}")
        embed.add_field(name="Information", value=information_links)

        embed.add_field(name="Notes",
                        value="* These values may be slightly wrong due to floating point errors.",
                        inline=False)
        embed.set_footer(text="Thumbnail provided by the COLOURlovers API")
        await ctx.send(embed=embed)


def setup(bot):
    """Sets up the cog."""
    bot.add_cog(Color())
