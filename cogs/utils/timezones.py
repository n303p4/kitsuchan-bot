"""A time command."""

from datetime import datetime
import pytz
from pytz.exceptions import UnknownTimeZoneError

from discord.ext import commands

URL = "https://en.wikipedia.org/wiki/List_of_tz_database_time_zones"
FORMAT_TIME = "%-I:%M:%S %p %Z (UTC%z)"
FORMAT_DATE = "%Y-%m-%d %Z"
TIMEZONE_US_EASTERN = pytz.timezone("US/Eastern")
CONTINENTS = ["Africa/", "America/", "America/Argentina/", "America/Indiana/", "America/Kentucky/",
              "Antarctica/", "Asia/", "Australia/", "Europe/", "Indian/", "Pacific/", "Etc/", ""]


class Time(commands.Cog):
    """Time-related commands."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def daysago(self, ctx, year: int, month: int, day: int):
        """
        A command that tells you how many days ago a date was, in US Eastern Time.

        Example usage:
        * time 2015 2 21
        """
        utcmoment = datetime.now(tz=TIMEZONE_US_EASTERN)
        input_time = datetime(year, month, day).astimezone(TIMEZONE_US_EASTERN)
        num_days_ago = (utcmoment - input_time).days
        await ctx.send(f"{input_time.strftime(FORMAT_DATE)} was {num_days_ago} days ago.")

    @commands.command(aliases=["timein", "tz", "timezone"])
    @commands.cooldown(6, 12)
    async def time(self, ctx, *, location: str):
        """A command that fetches the time in a given area using TZ database names.

        Tries to do simple guesswork to accommodate invalid inputs.

        Example usage:
        * time Europe/Berlin
        * time UTC
        * time buenos aires
        """
        try:
            timezone = location.replace(" ", "_")
            if not "/" in timezone:
                if timezone.upper() != timezone:
                    timezone = timezone.title()
                for continent in CONTINENTS:
                    try:
                        timezone = pytz.timezone(f"{continent}{timezone}")
                    except UnknownTimeZoneError:
                        continue
                    else:
                        break
                if isinstance(timezone, str):
                    raise UnknownTimeZoneError()
            else:
                timezone = pytz.timezone(location)
        except UnknownTimeZoneError:
            raise commands.UserInputError(("Could not determine TZ database name from input. "
                                           f"Please refer to <{URL}> for a complete list of "
                                           "TZ database names.\n\n"
                                           "Tip 1: Make sure you've spelled the location name "
                                           "properly. For example, `new york` instead of "
                                           "`new yorj`.\n"
                                           "Tip 2: Major city names are more likely to work than "
                                           "country names.\n"
                                           "Tip 3: The command tries to do some guesswork to fill "
                                           "in blanks; for example simply giving `buenos aires` "
                                           "will result in `America/Argentina/Buenos_Aires`."))
        utcmoment = datetime.utcnow().replace(tzinfo=pytz.utc)
        localized_datetime = utcmoment.astimezone(timezone)
        human_readable_location = timezone.zone.split("/")[-1].replace("_", " ")
        message = (f"The time in {human_readable_location} is currently "
                   f"{localized_datetime.strftime(FORMAT_TIME)}.")
        await ctx.send(message)

def setup(bot):
    """Set up the extension."""
    bot.add_cog(Time())
