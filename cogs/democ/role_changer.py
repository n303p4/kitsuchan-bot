"""Edit a user's roles."""

import discord
from discord.ext import commands

import webcolors


class NoUniqueRoleWithColor(commands.CommandError):
    """Raise if a user lacks a role that is unique to them and has a color."""


def color_from_string(color):
    """Given a string, get color info."""
    try:
        color = webcolors.name_to_hex(color)
    except (ValueError, AttributeError):
        pass
    try:
        color = color.lstrip("#")  # Remove the pound sign.
        color = int(f"0x{color}", 16)
        color = discord.Color(color)
    except ValueError:
        raise commands.UserInputError(("Not a valid color. "
                                       "Color must either be A) in hex format (e.g. `808080`)"
                                       " and between `FFFFFF` and `000000`, or B) A named CSS"
                                       " color (e.g. `red` or `lightsteelblue`)."))
    return color


def get_unique_role_with_color(member):
    """Get a role that the user has and nobody else has."""
    for role in reversed(member.roles):
        if role.color.value and len(role.members) == 1:
            return role
    raise NoUniqueRoleWithColor("Could not find a colored role that is unique to you.")


class RoleChanger(commands.Cog):
    """Edit a user's topmost colored role."""

    @commands.command(aliases=["rcolor"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def rolecolor(self, ctx, *, role_color: str):
        """
        Change your role color.

        Modifies the colored role with the topmost position that is **unique** to the user.
        """
        new_role_color = color_from_string(role_color)
        if new_role_color.value == 0:
            raise commands.UserInputError("Role color cannot be empty!")

        unique_role_with_color = get_unique_role_with_color(ctx.author)

        old_role_color = unique_role_with_color.color
        await unique_role_with_color.edit(colour=new_role_color)

        embed = discord.Embed()
        embed.colour = new_role_color

        embed.description = (f"Recolored role {unique_role_with_color} "
                             f"from {old_role_color} to {new_role_color}")

        await ctx.send(embed=embed)

    @commands.command(aliases=["rname"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def rolename(self, ctx, *, role_name: str):
        """
        Change your role name.

        Modifies the colored role with the topmost position that is **unique** to the user.
        """
        unique_role_with_color = get_unique_role_with_color(ctx.author)

        old_role_name = unique_role_with_color.name
        await unique_role_with_color.edit(name=role_name)

        await ctx.send(f"Renamed role __{old_role_name}__ to __{role_name}__")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(RoleChanger())
