"""Edit the title of a channel."""

import discord
from discord.ext import commands


class ThreadName(commands.Cog):
    """Edit the title of a channel."""

    @commands.command(aliases=["cn", "title", "threadname", "tn", "thread"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def channelname(self, ctx, *, channel_name: str):
        """
        Change the name of a Discord channel.

        As it is incredibly abusable, this command should not be used in a public bot, ever.
        """
        try:
            await ctx.message.delete()
        except discord.HTTPException:
            pass
        await ctx.send(f"**{channel_name}**")
        try:
            await ctx.channel.edit(name=channel_name)
        except discord.HTTPException as error:
            warning = await ctx.send(f"Could not update channel name. {error}")
            await warning.delete(delay=5)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(ThreadName())
