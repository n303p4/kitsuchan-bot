"""Note commands."""

# pylint: disable=unused-variable

import discord
from discord.ext import commands
from discord.ext.commands import BucketType

EMOJIS_DELETE = ["❌", "❎", "🇽"]


class NotePoster(commands.Cog):
    """Post notes to a specific designated channel."""

    def __init__(self, bot):
        self.bot = bot
        self.notes_textchannel = None

        bot.loop.create_task(self.init())

    async def init(self):
        """Initialize behavior."""
        await self.bot.wait_until_ready()

        notes_textchannel = self.bot.get_channel(self.bot.config.get("notes_textchannel_id"))

        if not isinstance(notes_textchannel, discord.TextChannel):
            return

        self.notes_textchannel = notes_textchannel

        @self.bot.listen("on_raw_reaction_add")
        async def delete_note(payload):
            """Delete a note."""
            channel = self.bot.get_channel(payload.channel_id)
            if channel != self.notes_textchannel:
                return
            if payload.emoji.name in EMOJIS_DELETE:
                message = await channel.fetch_message(payload.message_id)
                try:
                    await message.delete()
                except discord.HTTPException:
                    pass

    @commands.command(aliases=["n"])
    @commands.cooldown(1, 2, type=BucketType.user)
    async def note(self, ctx, *, note_text: str):
        """Post something to a designated notes channel. You may react to a note with an "X" to delete it.

        Notes channel must be configured in the bot's config.
        """
        try:
            await ctx.message.delete()
        except discord.HTTPException:
            pass

        if not self.notes_textchannel:
            await ctx.send((
                "Note command is not configured correctly! Please ensure that `notes_textchannel_id` "
                "is set in the bot's config."
            ))
            return
        embed = discord.Embed(description=note_text.strip()[:2048])
        embed.set_author(
            name=f"{ctx.author.display_name} ({ctx.author} | {ctx.author.id})",
            icon_url=ctx.author.avatar_url_as(size=32)
        )
        await self.notes_textchannel.send(embed=embed)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(NotePoster(bot))
