"""Edit a user's nickname."""

import discord
from discord.ext import commands


class Nickname(commands.Cog):
    """Edit a user's nickname."""

    @commands.command(aliases=["nick"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def nickname(self, ctx, user: discord.Member, *, nickname: str):
        """Change someone's nickname."""
        await user.edit(nick=nickname)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Nickname())
