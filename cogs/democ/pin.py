"""Message pinning and unpinning behaviors and commands."""

# pylint: disable=unused-variable

import discord
from discord.ext import commands

EMOJIS_PINS = ["📌", "📍"]


async def init_behavior(bot):
    """Set up automatic pinning behavior."""
    await bot.wait_until_ready()

    @bot.listen("on_raw_reaction_add")
    async def pin_message(payload):
        """Automatically pin a message if the :pushpin: or :round_pushpin: react is added."""
        if payload.emoji.name in EMOJIS_PINS:
            channel = bot.get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            try:
                await message.pin()
            except discord.HTTPException:
                pass

    @bot.listen("on_raw_reaction_remove")
    async def unpin_message(payload):
        """Automatically unpin a message if the :pushpin: or :round_pushpin: react is removed."""
        if payload.emoji.name in EMOJIS_PINS:
            channel = bot.get_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            try:
                await message.unpin()
            except discord.HTTPException:
                pass


class Pin(commands.Cog):
    """Pin a post."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def pin(self, ctx):
        """
        Pins the post that initiated this command.

        You can also pin a message by reacting to it with :pushpin: or :round_pushpin:
        """

        await ctx.message.pin()

    @commands.command()
    @commands.cooldown(6, 12)
    async def unpin(self, ctx, *, message_id: int = None):
        """
        Unpins a message by ID. The message must be in the current channel.

        If no ID is specified, then the most recently pinned message is unpinned.

        You can also unpin a message by removing :pushpin: or :round_pushpin: reactions from it.
        """

        try:
            if message_id:
                message = await ctx.channel.fetch_message(message_id)
            else:
                message = (await ctx.channel.pins())[0]
            if not message.pinned:
                await ctx.send(f"Message at {message.jump_url}")
                return
            await message.unpin()
            await ctx.send(f"Unpinned message at {message.jump_url}")
        except discord.HTTPException:
            await ctx.send(f"No message with ID {message_id} was found in this channel.")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Pin())
    bot.loop.create_task(init_behavior(bot))
