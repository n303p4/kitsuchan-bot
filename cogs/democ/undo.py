"""Undo the last thing posted by the bot."""

from datetime import datetime, timedelta

import discord
from discord.ext import commands


EMOJIS_DELETE = ["❌"]


async def init_behavior(bot):
    """Set up automatic pinning behavior."""
    await bot.wait_until_ready()

    @bot.listen("on_raw_reaction_add")
    async def undo_message(payload):
        """Automatically pin a message if it the :x: react is added."""
        if payload.emoji.name in EMOJIS_DELETE:
            channel = await bot.fetch_channel(payload.channel_id)
            message = await channel.fetch_message(payload.message_id)
            if message.author.id == bot.user.id:
                await delete_message_within(message, 5)


async def get_last_post_by(channel, user):
    """Retrieve the last thing posted in a channel by user.

    Returns the `Message` object it found, or `None` if nothing is found.

    * `channel` - The channel to search for images in.
    * `user` - The user to match.
    """
    message = None
    async for message in channel.history():
        if message.author == user:
            return message


async def delete_message_within(message, interval: int = 5):
    """Delete a message that was sent within interval # of minutes."""
    now = datetime.utcnow()
    if now - message.created_at < timedelta(minutes=interval):
        await message.delete()
        return
    warning = await message.channel.send("Can only undo a post from within the last 5 minutes.")
    await warning.delete(delay=8)


class Undo(commands.Cog):
    """Undo the last thing posted by the bot."""

    @commands.command(aliases=["z"])
    @commands.cooldown(6, 12)
    async def undo(self, ctx):
        """
        Remove the last thing that was posted by the bot.

        Only works on messages that are within 5 minutes old.
        """
        try:
            await ctx.message.delete()
        except discord.HTTPException:
            pass
        await delete_message_within(await get_last_post_by(ctx.channel, ctx.bot.user), 5)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Undo())
    bot.loop.create_task(init_behavior(bot))
