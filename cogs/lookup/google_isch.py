"""This cog contains an image query command."""

import secrets
from urllib.parse import urljoin, urlencode, urlparse, parse_qs

from bs4 import BeautifulSoup
import discord
from discord.ext import commands

from kitsuchan.exceptions import WebAPIUnreachable, WebAPINoResultsFound, WebAPIInvalidResponse

BASE_URL_GOOGLE = "https://www.google.com"
BASE_URL_GOOGLE_ISCH = "https://www.google.com/search?{0}"
WEBSITE_TEXT = "Website for this image"
USER_AGENT = ("Opera/9.80 (J2ME/MIDP; Opera Mini/(Windows; U; Windows NT 5.1; en-US) AppleWebKit/23.411; U; en) "
              "Presto/2.5.25 Version/10.54")
SEARCH_HEADERS = {"User-Agent": USER_AGENT}


async def _google_isch_get_one(session, query: str, search_headers: dict=None):
    """Google image search function that gets a random image result."""

    if not search_headers:
        search_headers = SEARCH_HEADERS

    params = urlencode({"tbm": "isch", "q": query})
    url = BASE_URL_GOOGLE_ISCH.format(params)
    async with session.get(url, headers=search_headers) as response:
        if response.status < 400:
            xml = await response.text()
            soup = BeautifulSoup(xml, features="lxml")
            results = soup.find_all("a", href=lambda h: h.startswith("/imgres"))
            if not results:
                raise WebAPINoResultsFound(message="No images found.")
            result = secrets.choice(results)["href"]
            return urljoin(BASE_URL_GOOGLE, result)
        else:
            raise WebAPIUnreachable(service="Google Images")


async def _google_isch_handle_one(session, url, search_headers: dict=None):
    """Google image search function that parses an individual result."""

    if not search_headers:
        search_headers = SEARCH_HEADERS

    async with session.get(url, headers=search_headers) as response:
        if response.status < 400:
            xml = await response.text()
            soup = BeautifulSoup(xml, features="lxml")
            thumbnail = soup.find("a", id="thumbnail", href=True)
            if not thumbnail:
                raise WebAPIInvalidResponse(service="Google Images")
            website_link = soup.find("a", href=True, text=WEBSITE_TEXT)
            if not website_link:
                raise WebAPIInvalidResponse(service="Google Images")
            image_url = thumbnail["href"]
            try:
                website_url = parse_qs(urlparse(website_link["href"]).query)["q"][0]
            except Exception as error:
                raise WebAPIInvalidResponse(service="Google Images") from error
            return image_url, website_url
        else:
            raise WebAPIUnreachable(service="Google Images")


class GoogleImages(commands.Cog):
    """Google image search."""

    def __init__(self, search_headers=None):
        self.search_headers = search_headers

    @commands.command(aliases=["img", "gimage", "gimg"])
    @commands.cooldown(6, 12)
    async def image(self, ctx, *, query: str):
        """Get a random image off the Internet using Google Images.

        * query - A string to be used in the search criteria.
        """
        search_result_url = await _google_isch_get_one(ctx.bot.session, query, self.search_headers)
        image_url, website_url = await _google_isch_handle_one(ctx.bot.session, search_result_url, self.search_headers)
        embed = discord.Embed(title="Google Image Result")
        embed.url = search_result_url
        embed.description = (f"[{WEBSITE_TEXT}]({website_url})\n"
                             f"[Full-size image]({image_url})")
        embed.set_image(url=image_url)
        embed.set_footer(text="Powered (but not endorsed) by Google Images")
        await ctx.send(embed=embed)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(GoogleImages(search_headers=SEARCH_HEADERS))
