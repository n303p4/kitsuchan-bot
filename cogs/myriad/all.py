"""Myriad module."""

from myriad import cogs


def setup(bot):
    """Set up the cogs."""
    cogs.setup(bot)
