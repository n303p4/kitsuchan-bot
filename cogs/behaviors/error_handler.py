"""A simple error handling extension. Should work with any discord.ext-based bot."""

from discord.ext import commands
from discord.ext.commands import CommandNotFound, CommandOnCooldown, DisabledCommand, \
                                 CheckFailure, BadArgument, ExtensionError

from kitsuchan.exceptions import WebAPIInvalidResponse, WebAPIUnreachable

EXCEPTIONS_NO_PROMPT_HELP = (
    CommandNotFound,
    CommandOnCooldown,
    DisabledCommand,
    CheckFailure,
    BadArgument,
    ExtensionError,
    WebAPIInvalidResponse,
    WebAPIUnreachable
)


class IsNotHuman(commands.CommandError):
    """Raised if a bot attempts to invoke one of this bot's commands."""


def setup(bot):
    """Set up the cog."""

    @bot.check
    def is_human(ctx):
        """Prevent the bot from responding to other bots."""
        if ctx.author.bot:
            raise IsNotHuman("User is not human")
        return True

    @bot.listen("on_command_error")
    async def handle_error(ctx, exc):
        """Simple error handler."""
        if ctx.invoked_with and not ctx.invoked_with[0].isalpha():
            return
        if isinstance(exc, IsNotHuman):
            return
        if hasattr(exc, "original"):
            error_message = str(exc.original)
        else:
            error_message = str(exc)
        if not isinstance(exc, EXCEPTIONS_NO_PROMPT_HELP):
            error_message = (f"{error_message}\nYou may also want to try `{ctx.prefix}help {ctx.invoked_with}` "
                             "for more details on usage.")
        await ctx.send(error_message)
