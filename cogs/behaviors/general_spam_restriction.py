"""Cog that restricts bot commands to general and spam."""

import discord
from discord.ext import commands


class NotAllowedOutsideGeneralOrSpam(commands.CommandError):
    """Raised if a bot attempts to invoke one of this bot's commands."""


class GeneralSpamRestriction(commands.Cog):
    """Allow or disallow a command to be used outside general and spam."""

    @commands.command(aliases=["lgs"])
    @commands.is_owner()
    async def limitgeneralspam(self, ctx, *, command_name: str):
        """Restrict a command to general and spam."""
        if command_name in ctx.bot.config["allow_outside_general_spam"]:
            ctx.bot.config["allow_outside_general_spam"].remove(command_name)
            ctx.bot.save_config()
            await ctx.send(f"`{command_name}` limited to general and spam")
            return
        await ctx.send(f"`{command_name}` already limited to general and spam")

    @commands.command(aliases=["ulgs"])
    @commands.is_owner()
    async def unlimitgeneralspam(self, ctx, *, command_name: str):
        """Allow a command outside of general and spam."""
        if command_name not in ctx.bot.config["allow_outside_general_spam"]:
            ctx.bot.config["allow_outside_general_spam"].append(command_name)
            ctx.bot.save_config()
            await ctx.send(f"`{command_name}` allowed outside general and spam")
            return
        await ctx.send(f"`{command_name}` already allowed outside general and spam")


def setup(bot):
    """Restrict commands to general and spam, unless whitelisted."""

    bot.config.setdefault("allow_outside_general_spam", [])
    bot.config.setdefault("general_spam_channels", [])

    @bot.check
    def restrict_to_general_and_spam(ctx):
        """Limit bot to general and spam."""
        general_spam_channels = ctx.bot.config.get("general_spam_channels", [])
        if not isinstance(general_spam_channels, list):
            general_spam_channels = []
        if (not isinstance(ctx.channel, discord.TextChannel) or
                ctx.channel.id in general_spam_channels):
            return True
        whitelist = ctx.bot.config.get("allow_outside_general_spam", [])
        if not isinstance(whitelist, list):
            whitelist = []
        if ctx.command.name in whitelist:
            return True
        raise NotAllowedOutsideGeneralOrSpam("Command is restricted to general and spam.")

    bot.add_cog(GeneralSpamRestriction())
