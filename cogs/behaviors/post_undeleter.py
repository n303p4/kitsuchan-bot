"""Automatically repost deleted messages."""

# pylint: disable=unused-variable

from datetime import datetime

import discord
from discord.ext.commands import Paginator


async def init_behavior(bot):
    """Define the behavior in here."""
    await bot.wait_until_ready()

    channel_whitelist = bot.config.get("post_undeleter_whitelisted_channels", [])

    @bot.listen("on_message_delete")
    async def handle_message_delete(message):
        """Automatically repost deleted messages."""
        now = datetime.utcnow()
        if message.author.id == bot.user.id \
           or not message.guild \
           or message.channel.id in channel_whitelist:
            return
        target_guild = message.guild
        target_channel = message.channel
        async for entry in target_guild.audit_logs(limit=1):
            delta = abs((entry.created_at - now).total_seconds())
            # SUPER CURSED. We have to just guess, pretty much.
            if entry.action == discord.AuditLogAction.message_delete \
               and entry.target.id == message.author.id \
               and entry.extra.channel.id == target_channel.id \
               and (entry.extra.count == 1 or delta <= 300):
                paginator = Paginator(prefix="", suffix="")
                paginator.add_line(f"At {message.created_at} UTC, {message.author.mention} said:")
                for line in message.content.split("\n"):
                    paginator.add_line(line)
                for page in paginator.pages:
                    message_part = await target_channel.send(page)
                    if message.pinned:
                        try:
                            await message_part.pin()
                        except discord.HTTPException:
                            pass


def setup(bot):
    """Set up the cog."""
    bot.loop.create_task(init_behavior(bot))
