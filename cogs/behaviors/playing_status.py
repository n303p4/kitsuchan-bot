"""This extension sets the bot's playing status."""

from asyncio import sleep

import discord
from discord.ext import commands


class PlayingStatus(commands.Cog):
    """Bot playing status manager."""

    def __init__(self, bot):
        self.bot = bot
        self.playing_status_task = self.bot.loop.create_task(
            self.update_playing_status_every_30_minutes()
        )

    async def update_playing_status_every_30_minutes(self):
        """Update playing status every 30 minutes."""
        await self.bot.wait_until_ready()
        while True:
            await self.update_playing_status()
            await sleep(60 * 30)

    async def update_playing_status(self):
        """Set playing status to some user-defined text."""
        playing_status = self.bot.config.get("playing_status")

        if not playing_status:
            display_prefix = self.bot.config.get("prefix", f'@{self.bot.user.name}')
            if len(display_prefix) > 1:  # No space used for single-character prefixes.
                display_prefix += " "
            playing_status = f"Type {display_prefix}help for help!"

        game = discord.Game(name=playing_status)
        await self.bot.change_presence(status=discord.Status.online, activity=game)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(PlayingStatus(bot))
