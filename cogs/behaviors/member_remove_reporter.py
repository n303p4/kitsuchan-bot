"""Check if a user has left the guild and report it."""

import discord


async def init_reporter(bot):
    """Define the behavior in here."""

    await bot.wait_until_ready()

    home_guild_id = bot.config.get("home_guild_id")
    home_guild = None
    for guild in bot.guilds:
        if guild.id == home_guild_id:
            home_guild = guild
            break

    if not home_guild:
        return

    home_textchannel_id = bot.config.get("home_textchannel_id")
    home_textchannel = None
    for channel in home_guild.channels:
        if channel.id == home_textchannel_id:
            home_textchannel = channel
            break

    if not isinstance(home_textchannel, discord.TextChannel):
        return

    @bot.listen("on_member_remove")
    async def handle_member_remove(member):
        """Simple member remove check."""
        if member.guild == home_guild:
            await home_textchannel.send(f"{member} has exited the guild.")


def setup(bot):
    """Set up the cog."""

    bot.loop.create_task(init_reporter(bot))
