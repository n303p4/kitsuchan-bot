"""
Map commands, for role-playing.

A tile is an immovable part of a map; a token is a movable part.
"""

# pylint: disable=broad-except

# TODO: When discord.py supports buttons, use those instead of reactions
# TODO: Add persistence for maps

from asyncio import sleep
from datetime import datetime, timedelta
import re
import string
import unicodedata

import discord
from discord.ext import commands

MAX_WIDTH = 14
MAX_COLUMN_LETTER = chr(64+MAX_WIDTH)
MAX_HEIGHT = 49

REACTIONS_DIRECTIONS = ["⬅", "⬆️", "⬇", "➡️", "🇽"]
REACTIONS_SPACING = ["⬛", "◼️", "◾", "▪️"]
EMOJI_TILE_EMPTY = "⬛"
EMOJI_TILE_OBJECT = "⬜"
EMOJI_TILES_SPECIAL = {
    ".": EMOJI_TILE_EMPTY,
    "$": "💵",
    "!": "❗",
    "?": "❔",
    "*": "✳️",
    "_": EMOJI_TILE_EMPTY,
    "\u20e3": EMOJI_TILE_OBJECT
}
EMOJI_TOKENS = ["🔵", "🔷", "💙", "📘", "🎵", "🌀"]

REGEX_COORDINATE = r"([a-zA-Z])([1-9][0-9]*)(\-?.)?"
REGEX_OBJECT_COORDINATE = re.compile(REGEX_COORDINATE)

REGEX_COORDINATE_RANGE = r"([a-zA-Z])([1-9][0-9]*)\:([a-zA-Z])([1-9][0-9]*)(\-?.)?"
REGEX_OBJECT_COORDINATE_RANGE = re.compile(REGEX_COORDINATE_RANGE)


def column_number_from_letter(letter: str):
    """Convert a single letter to its integer form. A = 1, B = 2, etc."""
    return ord(letter.upper())-64


def emoji_for_letter(letter: str):
    """Get the appropriate emoji for a given letter."""
    return f":regional_indicator_{letter.lower()}:"


def emoji_for_digit(digit: str):
    """Get the appropriate emoji for a given ASCII digit."""
    return f"{digit}\u20e3"


def emoji_for_character(character: str, endrow: str = ""):
    """Return the emoji for a character."""
    if isinstance(character, str):
        if len(character) > 1:
            character = character[-1]
        if character == "\ufe0f" and len(endrow) > 1 and endrow[-1] in string.digits:
            tile = emoji_for_digit(endrow[-1])
            endrow = endrow[:-1]
        elif character in EMOJI_TILES_SPECIAL:
            tile = EMOJI_TILES_SPECIAL[character]
        elif character in string.ascii_letters:
            tile = emoji_for_letter(character)
        elif character in string.digits:
            tile = emoji_for_digit(character)
        elif character in string.printable:
            tile = EMOJI_TILE_OBJECT
        else:
            tile = character
    else:
        tile = EMOJI_TILE_OBJECT
    return tile, endrow


def parse_coordinate(coordinate_string):
    """Parse a coordinate string."""
    match = REGEX_OBJECT_COORDINATE.match(coordinate_string)
    if not match:
        return None, None
    tile_letter = match.groups()[2]
    tile, rownum = emoji_for_character(tile_letter, match.groups()[1])
    return (column_number_from_letter(match.groups()[0]), int(rownum)), tile


def parse_coordinate_range(coordinate_string):
    """Parse a coordinate string."""
    match = REGEX_OBJECT_COORDINATE_RANGE.match(coordinate_string)
    if not match:
        return None, None, None
    tile_letter = match.groups()[4]
    tile, endrow = emoji_for_character(tile_letter, match.groups()[3])
    startcol = column_number_from_letter(match.groups()[0])
    endcol = column_number_from_letter(match.groups()[2])
    if startcol > endcol:
        startcol, endcol = endcol, startcol
    startrow = int(match.groups()[1])
    endrow = int(endrow)
    if startrow > endrow:
        startrow, endrow = endrow, startrow
    return (startcol, startrow), (endcol, endrow), tile


class UserMapToken:
    """A token for display on a UserMap."""

    def __init__(self, member: discord.Member, emoji: str):
        self.member = member
        self.column = 0
        self.row = 0
        self.emoji = emoji


class UserMap:
    """A map with a list of users."""

    def __init__(self, width: int, height: int):
        self._created_at = datetime.utcnow()
        self.width = width
        self.height = height
        self.tokens = {}
        self.terrain = {}
        self.has_update = False
        self._edited_at = None

    @property
    def created_at(self):
        """Return the date that this UserMap was created."""
        return self._created_at

    @property
    def edited_at(self):
        """Return the date that this UserMap was last modified."""
        return self._edited_at

    def add_tile(self, column: int, row: int, tile: str):
        """Add tile to terrainS."""
        self.terrain[(column, row)] = tile

    def add_token(self, member: discord.Member):
        """Add movable token to map."""
        emoji = EMOJI_TOKENS[len(self.tokens) % len(EMOJI_TOKENS)]
        self.tokens[member.id] = UserMapToken(member, emoji)

    def set_emoji_for_token(self, id_, emoji: str):
        """Set an emoji for a given token."""
        if id_ not in self.tokens:
            return
        self.tokens[id_].emoji = emoji
        self.has_update = True
        self._edited_at = datetime.utcnow()

    def move_token(self, id_, column: int, row: int):
        """Move a token to a different location on the map."""
        if id_ not in self.tokens:
            return
        column = max(min(self.width-1, column), 0)
        row = max(min(self.height-1, row), 0)
        self.tokens[id_].column = column
        self.tokens[id_].row = row
        self.has_update = True
        self._edited_at = datetime.utcnow()

    def move_token_by_x(self, id_, value: int):
        """Move a user along the X axis."""
        if id_ not in self.tokens:
            return
        column = self.tokens[id_].column + value
        row = self.tokens[id_].row
        self.move_token(id_, column, row)

    def move_token_by_y(self, id_, value: int):
        """Move a user along the Y axis."""
        if id_ not in self.tokens:
            return
        column = self.tokens[id_].column
        row = self.tokens[id_].row + value
        self.move_token(id_, column, row)

    def remove_token(self, id_):
        """Remove a user from the map."""
        if id_ in self.tokens:
            del self.tokens[id_]
            self.has_update = True
            self._edited_at = datetime.utcnow()

    def to_emojis(self):
        """Render this map as a grid of emojis."""
        rows = []
        for rownum in range(0, self.height):
            row = []
            for colnum in range(0, self.width):
                for token in self.tokens.values():
                    if (token.column, token.row) == (colnum, rownum):
                        row.append(token.emoji)
                        break
                else:
                    if (colnum, rownum) in self.terrain:
                        row.append(self.terrain[(colnum, rownum)])
                    else:
                        row.append(EMOJI_TILE_EMPTY)
            rows.append("\u200b".join(row))
        return rows


class Map(commands.Cog):
    """Commands that produce random outputs."""

    def __init__(self, bot):
        super(Map, self).__init__()
        self.usermaps_by_channel = {}
        self.member_converter = commands.MemberConverter()

        self.bot = bot
        self.bot.loop.create_task(self.create_listeners())
        self.update_usermaps_task = self.bot.loop.create_task(self.update_usermaps_loop())
        self.delete_usermaps_task = self.bot.loop.create_task(self.delete_usermaps_loop())

    async def create_listeners(self):
        """Set up movement."""
        await self.bot.wait_until_ready()

        @self.bot.listen("on_raw_reaction_add")
        async def move1(payload):
            """Automatically pin a message if the :pushpin: or :round_pushpin: react is added."""
            await self.update_usermap(payload)

        @self.bot.listen("on_raw_reaction_remove")
        async def move2(payload):
            """Automatically unpin a message if the :pushpin: or :round_pushpin: react is removed."""
            if payload.emoji.name in REACTIONS_DIRECTIONS:
                await self.update_usermap(payload)

    async def update_usermaps_loop(self):
        """Update all maps on a loop every second."""
        await self.bot.wait_until_ready()
        while True:
            try:
                for channel_id, usermaps in self.usermaps_by_channel.items():
                    channel = self.bot.get_channel(channel_id)
                    if not channel:
                        continue
                    maps_to_delete = []
                    for message_id, usermap in usermaps.items():
                        if not usermap.tokens:
                            continue
                        if not usermap.has_update:
                            continue
                        message = channel.get_partial_message(message_id)
                        content = "\n".join(usermap.to_emojis())
                        try:
                            await message.edit(content=content)
                        except discord.NotFound:
                            maps_to_delete.append(message_id)
                        except Exception:
                            pass
                        else:
                            usermap.has_update = False
                    for message_id in maps_to_delete:
                        try:
                            del self.usermaps_by_channel[channel_id][message_id]
                        except KeyError:
                            pass
                        else:
                            print("Usermap for", message_id, "was deactivated")
            except Exception:
                pass
            await sleep(1)

    async def delete_usermaps_loop(self):
        """Stop responding to maps that are over 24 hours old."""
        await self.bot.wait_until_ready()
        while True:
            utcnow = datetime.utcnow()
            try:
                channels_to_delete = []
                for channel_id, usermaps in self.usermaps_by_channel.items():
                    channel = self.bot.get_channel(channel_id)
                    if not channel or not usermaps:
                        channels_to_delete.append(channel_id)
                        continue
                    maps_to_delete = []
                    for message_id, usermap in usermaps.items():
                        if utcnow - usermap.edited_at > timedelta(hours=2):
                            maps_to_delete.append(message_id)
                        elif not usermap.tokens:
                            maps_to_delete.append(message_id)
                    for message_id in maps_to_delete:
                        try:
                            del self.usermaps_by_channel[channel_id][message_id]
                        except KeyError:
                            pass
                        else:
                            print("Usermap for", message_id, "was deactivated")
                for channel_id in channels_to_delete:
                    try:
                        del self.usermaps_by_channel[channel_id]
                    except KeyError:
                        pass
                    else:
                        print(channel_id, "has no active usermaps")
            except Exception:
                pass
            await sleep(30)

    async def update_usermap(self, payload):
        """Update a known map."""
        try:
            channel = self.bot.get_channel(payload.channel_id)
            message = channel.get_partial_message(payload.message_id)
            member = self.bot.get_guild(payload.guild_id).get_member(payload.user_id)
        except Exception:
            return
        if not isinstance(self.usermaps_by_channel.get(channel.id), dict):
            return
        if message.id not in self.usermaps_by_channel[channel.id]:
            return
        usermap = self.usermaps_by_channel[channel.id][message.id]
        if member.id not in usermap.tokens:
            return
        if payload.emoji.name == REACTIONS_DIRECTIONS[1]:
            usermap.move_token_by_y(member.id, -1)
        elif payload.emoji.name == REACTIONS_DIRECTIONS[2]:
            usermap.move_token_by_y(member.id, 1)
        elif payload.emoji.name == REACTIONS_DIRECTIONS[0]:
            usermap.move_token_by_x(member.id, -1)
        elif payload.emoji.name == REACTIONS_DIRECTIONS[3]:
            usermap.move_token_by_x(member.id, 1)
        elif payload.emoji.name == REACTIONS_DIRECTIONS[4]:
            usermap.remove_token(member.id)
        elif payload.emoji.name not in REACTIONS_SPACING:
            usermap.set_emoji_for_token(member.id, str(payload.emoji))
        try:
            if not usermap.tokens:
                await message.delete()
        except discord.HTTPException:
            pass

    # pylint: disable=missing-function-docstring
    @commands.command(aliases=["um"], help=(
        "Generate a user-controllable map with one or more users.\n\n"

        "`args` is a list of users and spreadsheet coordinates. Users will be assigned a movable token, and can "
        "optionally be followed by a colon and a spreadsheet coordinate to change their starting position. Raw "
        "spreadsheet coordinates and ranges of coordinates become terrain tiles, which cannot be moved. Run "
        "`help map` for more details on the syntax.\n\n"

        "Users can be referenced by any of: ID, mention, username#discriminator, username, or nickname. Surround "
        "the argument with quotation marks if there is a space in the name, e.g. `\"Sir Fluffery:d3\"`.\n\n"

        "If you have a token on the map, you can react with ⬅⬆️⬇➡️ to move your token around the map. React with 🇽 "
        "to delete your token. You can also react with any other emoji to change your token's emoji. The map will "
        "stop responding to reactions if it goes for 2 hours without anyone interacting with it.\n\n"

        """Example:
        `usermap 8 8 Kitsuchan:c2 b6 d3:e5x "Sir Fluffery:b1"`

        Output: Creates the following map:
        ⬛🔷⬛⬛⬛⬛⬛⬛
        ⬛⬛🔵⬛⬛⬛⬛⬛
        ⬛⬛⬛🇽\u200b🇽\u200b⬛⬛⬛
        ⬛⬛⬛🇽\u200b🇽\u200b⬛⬛⬛
        ⬛⬛⬛🇽\u200b🇽\u200b⬛⬛⬛
        ⬛⬜⬛⬛⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛
        The 🔵 and 🔷 are the tokens for the users Kitsuchan and Sir Fluffery, respectively. Other tokens are terrain.
        """))
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def usermap(self, ctx, width: int, height: int, *args):
        members_to_add = {}
        bad_coordinates = []
        terrain_coordinates = {}
        for arg in args:
            try:
                argparts = arg.split(":", 1)
                member = await self.member_converter.convert(ctx, argparts[0])
                if len(argparts) == 2:
                    coordinate_string = argparts[1]
                    coordinate, token = parse_coordinate(coordinate_string)
                    if not coordinate:
                        token, _ = emoji_for_character(coordinate_string[0])
                else:
                    coordinate, token = None, None
            except Exception:
                pass
            else:
                if member.id == ctx.bot.user.id:
                    raise commands.UserInputError("As the hosting bot, I cannot participate. :<")
                members_to_add[member] = (coordinate, token)
                continue
            if ":" in arg:
                coordinate1, coordinate2, token = parse_coordinate_range(arg)
                if coordinate1 and coordinate2:
                    for colnum in range(coordinate1[0], coordinate2[0]+1):
                        for rownum in range(coordinate1[1], coordinate2[1]+1):
                            terrain_coordinates[(colnum, rownum)] = token
                else:
                    bad_coordinates.append(arg)
            else:
                coordinate, token = parse_coordinate(arg)
                if coordinate:
                    terrain_coordinates[coordinate] = token
                else:
                    bad_coordinates.append(arg)

        if not members_to_add:
            raise commands.UserInputError("You must specify at least one user.")
        if width > MAX_WIDTH or height > MAX_WIDTH:
            raise commands.UserInputError(f"Max width and height are both {MAX_WIDTH}.")

        message = await ctx.send("Generating map...")
        self.usermaps_by_channel.setdefault(ctx.channel.id, {})
        self.usermaps_by_channel[ctx.channel.id][message.id] = UserMap(width, height)

        for coordinate, token in terrain_coordinates.items():
            self.usermaps_by_channel[ctx.channel.id][message.id].add_tile(coordinate[0]-1, coordinate[1]-1, token)

        for colnum, (member, (coordinate, token)) in enumerate(members_to_add.items()):
            self.usermaps_by_channel[ctx.channel.id][message.id].add_token(member)
            if coordinate:
                self.usermaps_by_channel[ctx.channel.id][message.id].move_token(
                    member.id, coordinate[0]-1, coordinate[1]-1
                )
            else:
                self.usermaps_by_channel[ctx.channel.id][message.id].move_token(member.id, colnum, 0)
            if isinstance(token, str) and token != EMOJI_TILE_OBJECT:
                self.usermaps_by_channel[ctx.channel.id][message.id].set_emoji_for_token(member.id, token)

        content = "\n".join(self.usermaps_by_channel[ctx.channel.id][message.id].to_emojis())
        await message.edit(content=content)

        if bad_coordinates:
            bad_paginator = commands.Paginator(prefix="Bad coordinates:", suffix="", linesep=" ")
            for coordinate_string in bad_coordinates:
                bad_paginator.add_line(coordinate_string)
            for page in bad_paginator.pages:
                await ctx.send(page)

        for reaction in REACTIONS_DIRECTIONS[:-1] + REACTIONS_SPACING + [REACTIONS_DIRECTIONS[-1]]:
            await message.add_reaction(reaction)

    # pylint: disable=missing-function-docstring
    @commands.command(help=(
        "Make a tile map of emojis. Accepts spreadsheet coordinates (not case sensitive), optionally followed by a "
        "character to use for display. The map starts from A1 in the top left corner, where A is the column "
        "and 1 is the row. Map size is automatically based on the largest coordinates provided. "
        f"The maximum allowed coordinate is {MAX_COLUMN_LETTER}{MAX_HEIGHT}.\n\n"

        "Ranges are supported, e.g. `a1:b3x` creates a rectangle of Xs from A1 to B3. The order does not matter; "
        "`a1:b3`, `a3:b1`, `b3:a1`, and `b1:a3` all produce the same result.\n\n"

        "To display a number, either its emoji form (e.g. d11️⃣), or separate it from the coordinate with a `-` "
        "(e.g. `d1:d3-1`)\n\n"

        """If multiple coordinates are the same, then the one that is given last takes priority.

        Example 1:
        `map h8. b2 g3 d7a f6w`

        Output:
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛⬜⬛⬛⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬜⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛🇼⬛⬛
        ⬛⬛⬛🇦⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛\n"""
        "`h8.` is used to set the map size by putting an empty tile at H8. `d7a` and `f6w` create their respective "
        "letters at those coordinates.\n"
        """
        Example 2, using ranges:
        `map h8. b2:d5🔥 c6:g7`

        Output:
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛🔥🔥🔥⬛⬛⬛⬛
        ⬛🔥🔥🔥⬛⬛⬛⬛
        ⬛🔥🔥🔥⬛⬛⬛⬛
        ⬛🔥🔥🔥⬛⬛⬛⬛
        ⬛⬛⬜⬜⬜⬜⬜⬛
        ⬛⬛⬜⬜⬜⬜⬜⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛\n"""
        "A rectangle of fire is created from B2 to D5, inclusive, as well as "
        "a rectangle of plain tokens from C6 to G7, inclusive.\n"
        """
        Example 3, with coordinates and ranges overwriting other coordinates:
        `map h8. b2:d4 d4:e6l c3🔥 d7a g6f g3x g6🏙`

        Output:
        ⬛⬛⬛⬛⬛⬛⬛⬛
        ⬛⬜⬜⬜⬛⬛⬛⬛
        ⬛⬜🔥⬜⬛⬛🇽⬛
        ⬛⬜⬜\u200b🇱\u200b🇱\u200b⬛⬛⬛
        ⬛⬛⬛\u200b🇱\u200b🇱\u200b⬛⬛⬛
        ⬛⬛⬛\u200b🇱\u200b🇱\u200b⬛🏙⬛
        ⬛⬛⬛🇦⬛⬛⬛⬛
        ⬛⬛⬛⬛⬛⬛⬛⬛
        Notice how `g6f` doesn't make an F at G6 appear, because `g6🏙` comes later and overwrites it.
        """))
    @commands.cooldown(6, 12)
    async def map(self, ctx, *coordinate_strings):
        coordinates = {}
        bad_coordinates = []
        for coordinate_string in coordinate_strings:
            if ":" in coordinate_string:
                coordinate1, coordinate2, tile = parse_coordinate_range(coordinate_string)
                if coordinate1 and coordinate2:
                    for colnum in range(coordinate1[0], coordinate2[0]+1):
                        for rownum in range(coordinate1[1], coordinate2[1]+1):
                            coordinates[(colnum, rownum)] = tile
                else:
                    bad_coordinates.append(coordinate_string)
            else:
                coordinate, tile = parse_coordinate(coordinate_string)
                if coordinate:
                    coordinates[coordinate] = tile
                else:
                    bad_coordinates.append(coordinate_string)

        if coordinates:
            width = max(coordinates.keys(), key=lambda c: c[0])[0]
            height = max(coordinates.keys(), key=lambda c: c[1])[1]
            error_message = []
            if width > MAX_WIDTH:
                error_message.append(f"Maximum column is {MAX_COLUMN_LETTER}.")
            if height > MAX_HEIGHT:
                error_message.append(f"Maximum row is {MAX_HEIGHT}.")
            if error_message:
                raise commands.UserInputError(" ".join(error_message))
            paginator = commands.Paginator(prefix="", suffix="", max_size=211)
            for rownum in range(1,height+1):
                row = "\u200b".join(
                    (EMOJI_TILE_EMPTY if (colnum, rownum) not in coordinates else coordinates[(colnum, rownum)])
                    for colnum in range(1,width+1)
                )
                paginator.add_line(row)
            for page in paginator.pages:
                await ctx.send(page)

            if bad_coordinates:
                bad_paginator = commands.Paginator(prefix="Bad coordinates:", suffix="", linesep=" ")
                for coordinate_string in bad_coordinates:
                    bad_paginator.add_line(coordinate_string)
                for page in bad_paginator.pages:
                    await ctx.send(page)

        else:
            raise commands.UserInputError(("No valid coordinates supplied. "
                                           "Please use spreadsheet format followed by an optional single "
                                           "character indicator, e.g. a1, b2e"))


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Map(bot))
