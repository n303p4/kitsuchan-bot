"""An assorted collection of meme commands."""

# pylint: disable=C0103

import asyncio
import secrets

from discord.ext import commands

SUMMONABLES = [
    ("Red-Eyes Black Dragon", "https://i.imgur.com/MiWwmqq.png"),
    ("Blue-Eyes White Dragon", "https://i.imgur.com/7LVixO3.png"),
    ("Exodia the Forbidden One", "https://i.imgur.com/wHWP34x.png"),
    ("Fox Fire", "https://i.imgur.com/xUIQmwa.png"),
    ("Bujingi Fox", "https://i.imgur.com/vFWak5N.png"),
    ("Lunalight Crimson Fox", "https://i.imgur.com/ReMqPsa.png"),
    ("Majespecter Fox - Kyubi", "https://i.imgur.com/5Yu8KJ7.png")
]
PLAY_MESSAGES = [
    "Play with me? o.o",
    "Yay, let's play! Try using the help command for a list of commands~ :3"
]


class Rhino(commands.Cog):
    """Various meme commands."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def play(self, ctx):
        """Play a game!"""
        message = secrets.choice(PLAY_MESSAGES)
        await ctx.send(message)

    @commands.command(name="np", aliases=["noproblem"])
    @commands.cooldown(6, 12)
    async def np_(self, ctx):
        """No problem!"""
        await ctx.send("No problem! :3")

    @commands.command()
    @commands.cooldown(6, 12)
    async def pause(self, ctx):
        """Pause for a bit."""
        await ctx.send("...")
        await asyncio.sleep(5)
        await ctx.send("...? o.o")

    @commands.command()
    @commands.cooldown(6, 12)
    async def summon(self, ctx):
        """Summon a monster!"""
        choice = secrets.choice(SUMMONABLES)
        name = choice[0]
        image = choice[1]
        await ctx.send(f"I summon {name}!\n{image}")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Rhino())
