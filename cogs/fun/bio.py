"""Contains a biography command for original charactering requirements."""

import discord
from discord.ext import commands


def embed_from_dict(dictionary):
    """Convert a specially crafted dict into an embed."""
    embed = discord.Embed(title=dictionary.get("title"))
    embed.description = dictionary.get("description")
    if dictionary.get("thumbnail"):
        embed.set_thumbnail(url=dictionary["thumbnail"])
    if dictionary.get("image"):
        embed.set_image(url=dictionary["image"])
    for field in dictionary.get("fields", []):
        embed.add_field(name=field["name"], value=field["value"])
    return embed


class Bio(commands.Cog):
    """Your bot's profile."""

    def __init__(self, bot):
        self.bot = bot
        self.biography = self.bot.config.get("biography")

        if self.biography and isinstance(self.biography, dict):

            @commands.command(aliases=["biography"])
            @commands.cooldown(6, 12)
            async def bio(ctx):
                """A short biography about this bot."""
                embed = embed_from_dict(self.biography)
                await ctx.send(embed=embed)

            self.bot.add_command(bio)

    def __unload(self):
        self.bot.remove_command("bio")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Bio(bot))
