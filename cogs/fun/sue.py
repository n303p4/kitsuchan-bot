"""A command that sues someone or something."""

# pylint: disable=C0103

import secrets
import re

from discord.ext import commands


class Sue(commands.Cog):
    """This command sues somebody!"""

    @commands.command()
    @commands.cooldown(6, 12)
    async def sue(self, ctx, *, target):
        """Sue somebody!

        Example usage:

        * sue
        * sue a person
        """
        conjunctions = " because | for | over "
        parts = [part.strip() for part in re.split(conjunctions, target, 1, re.I)]
        if len(parts) > 1 and parts[1]:
            conjunction = re.search(conjunctions, target, re.I).group(0)
            target = parts[0]
            reason = f"{conjunction}**{parts[1]}**"
        else:
            reason = ""
        if target:
            target = f" {target}"
        amount = f"**${str(secrets.randbelow(999901) + 100)}**"
        message = f"I-I'm going to sue{target} for {amount}{reason}! o.o"
        await ctx.send(message)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Sue())
