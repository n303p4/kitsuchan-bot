"""This extension gives the bot some ability to love."""

# pylint: disable=C0103

import secrets

from discord.ext import commands

LOVE = ["heart", "heart_exclamation", "heart_eyes", "gift_heart"]


class Love(commands.Cog):
    """Something about love."""

    @commands.command()
    async def love(self, ctx):
        """This bot knows how to love."""
        heart = secrets.choice(LOVE)
        await ctx.send(f":{heart}:")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Love())
