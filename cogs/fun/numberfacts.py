"""Trivia module with a trivia command."""

# pylint: disable=C0103

import secrets

import async_timeout
from discord.ext import commands
from kitsuchan.exceptions import WebAPIUnreachable
from kitsuchan.helpers import input_number

URL_NUMBERS_API = "http://numbersapi.com/{0}/{1}"
OPTIONS_NUMBERS_API = ["math", "trivia"]


def generate_query_url(number, kind):
    """Given a number and a type of query, generate a query URL for the numbers API."""
    url = URL_NUMBERS_API.format(number, kind)
    return url


async def query(session, url):
    """Given a ClientSession and url, query the numbers API and get a fact."""
    try:
        async with async_timeout.timeout(10):
            async with session.get(url) as response:
                if response.status == 200:
                    response_content = await response.text()
                    return response_content
                raise WebAPIUnreachable(service="numbersapi.com")
    except Exception:
        raise WebAPIUnreachable(service="numbersapi.com")


class NumFact(commands.Cog):
    """Random fact about a number."""

    @commands.command(aliases=["numberfact", "number"])
    @commands.cooldown(12, 12, commands.BucketType.channel)
    async def numfact(self, ctx, number: int):
        """Display a random fact about a number."""
        kind = secrets.choice(OPTIONS_NUMBERS_API)
        url = generate_query_url(number, kind)
        fact = await query(ctx.bot.session, url)
        await ctx.send(fact)

    @commands.command(aliases=["numberguess", "numbertrivia"])
    @commands.cooldown(12, 12, commands.BucketType.channel)
    async def numguess(self, ctx):
        """Guess the number based on a fact about that number!"""
        number = secrets.randbelow(50) + 1
        url = generate_query_url(number, "trivia")
        fact = await query(ctx.bot.session, url)
        question = fact.replace(str(number), "Which number").replace(".", "?", 1)
        question += "\nEnter a number between 1 and 50."
        choice = await input_number(ctx, question, timeout=15, min_value=1, max_value=50)
        if choice == number:
            await ctx.send("Correct! :3")
        else:
            await ctx.send(f"Nope, the correct answer is {number}. :<")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(NumFact())
