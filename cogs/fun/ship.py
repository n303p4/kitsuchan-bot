"""A command that ships two things together."""

from discord.ext import commands


def to_int(string, *, encoding="utf-8", byteorder="big"):
    """Convert a string into a UTF-8 encoded bytestring and then convert that into an integer."""
    return int.from_bytes(string.encode(encoding), byteorder=byteorder)


class Ship(commands.Cog):
    """A command that rates a ship."""

    @commands.command()
    @commands.cooldown(6, 12)
    async def ship(self, ctx, first_item, second_item):
        """Ship two things together."""
        rating = abs(to_int(first_item) - to_int(second_item)) % 101
        await ctx.send(f"I rate this ship a {rating}/100!")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Ship())
