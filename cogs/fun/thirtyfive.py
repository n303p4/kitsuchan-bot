"""Commands inspired by Roadcrosser's bots."""

# pylint: disable=C0103

import secrets

from discord.ext import commands
from kitsuchan.helpers import member_by_substring

AAA = (
    "a",
    "A",
    "\u3041",
    "\u3042",
    "\u30A1",
    "\u30A2"
)
THROWABLE_OBJECTS = (
    "heart",
    "cat",
    "dog",
    "fox",
    "cookie",
    "croissant",
    "lollipop",
    "book",
    "tangerine"
)


class ThirtyFive(commands.Cog):

    """This cog is a meme."""

    @commands.command(aliases=["aa", "aaa"])
    @commands.cooldown(10, 5, commands.BucketType.channel)
    async def a(self, ctx):
        """Aaaaaaa!"""
        message = secrets.choice(AAA) * (secrets.randbelow(191) + 10)
        await ctx.send(message)

    @commands.command(aliases=["snipe"])
    @commands.guild_only()
    @commands.cooldown(10, 5, commands.BucketType.user)
    async def throw(self, ctx, *, member):
        """Throw something at someone!"""
        member = await member_by_substring(ctx, member)
        if member == ctx.author:
            raise commands.UserInputError("You can't throw something at yourself. :<")
        elif member == ctx.bot.user:
            raise commands.UserInputError("Nope. :3")
        hit = secrets.randbelow(6)
        thing = secrets.choice(THROWABLE_OBJECTS)
        if hit:
            message = f":{thing}: {member.name} got a {thing} thrown at them! :3"
        else:
            message = f":{thing}: You missed! :3"
        await ctx.send(message)


def setup(bot):
    """Set up the 35 meme commands."""
    bot.add_cog(ThirtyFive())
