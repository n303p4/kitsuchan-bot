"""Contains a cog that handles tripmines, or in this case tripfoxes."""

# pylint: disable=C0103

import asyncio
import secrets

from discord.ext import commands
from kitsuchan.helpers import member_by_substring


class TripfoxChannelArray:
    """A list of tripfoxes for a given Discord channel."""

    def __init__(self):
        self.array = []

    def has_member(self, user_id: int):
        """Checks is a member is in the array."""
        return user_id in self.array

    def add_member(self, user_id: int):
        """Adds a member to the array."""
        if user_id not in self.array:
            self.array.append(user_id)

    def remove_member(self, user_id: int):
        """Removes a member from the array."""
        if user_id in self.array:
            self.array.remove(user_id)

    @property
    def is_empty(self):
        """The length of the array is 0."""
        return not self.array


class Tripfox(commands.Cog):
    """Set or remove a tripfox for someone."""

    def __init__(self, bot):

        self.tripfoxes = {}

        @bot.listen("on_message")
        async def _tripmine(message):
            """Listener for members in the chat who have been tripfoxed."""
            if message.author.id == bot.user.id:
                pass
            elif (message.channel.id in self.tripfoxes and
                  self.tripfoxes[message.channel.id].has_member(message.author.id)):
                explode = secrets.randbelow(10) + 1
                if explode == 1:
                    shiny = secrets.randbelow(4096) == 0
                    if shiny:
                        fox = ":sparkles::fox::sparkles:"
                        message = "You have been graced by the **shiny tripfox!**"
                    else:
                        fox = ":fox:"
                        message = "**RAWR!** You got attacked by a tripfox!"
                    await message.channel.send(f"{fox} {message.author.mention} {message} {fox}")
                    self.tripfoxes[message.channel.id].remove_member(message.author.id)
                    if self.tripfoxes[message.channel.id].is_empty:
                        del self.tripfoxes[message.channel.id]

    @commands.command(aliases=["tf", "mine", "setmine", "tripmine"])
    @commands.guild_only()
    @commands.cooldown(6, 12)
    async def tripfox(self, ctx, *, user: str):
        """Set a tripfox for someone. Tripfoxes will attack at random.

        * user - The person for which the fox will go off.
        """
        user = await member_by_substring(ctx, user)
        if user.id == ctx.bot.user.id:
            await ctx.send("Nope. :3")
        elif (ctx.channel.id in self.tripfoxes.keys() and
              self.tripfoxes[ctx.channel.id].has_member(user.id)):
            raise commands.UserInputError(f"A tripfox is already set for {user.display_name}.")
        else:
            self.tripfoxes.setdefault(ctx.channel.id, TripfoxChannelArray())
            self.tripfoxes[ctx.channel.id].add_member(user.id)
            message = await ctx.send(f"Tripfox set for {user.display_name}! :3")
            await asyncio.sleep(3)
            await message.delete()

    @commands.command(aliases=["utf", "unmine", "unsetmine", "removemine"])
    @commands.guild_only()
    @commands.cooldown(6, 12, commands.BucketType.channel)
    async def untripfox(self, ctx, *, user: str = None):
        """Remove a tripfox from yourself, or from someone else.

        * user - The person for which the fox will go off.
        """
        if not user:
            user = ctx.author
        else:
            user = await member_by_substring(ctx, user)

        if user.id == ctx.bot.user.id:
            await ctx.send("Nope. :3")
        elif (ctx.channel.id in self.tripfoxes.keys() and
              self.tripfoxes[ctx.channel.id].has_member(user.id)):
            self.tripfoxes[ctx.channel.id].remove_member(user.id)
            await ctx.send(f"Removed tripfox for {user.display_name}! :3")
        else:
            raise commands.UserInputError(f"No tripfox is currently set for {user.display_name}.")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Tripfox(bot))
