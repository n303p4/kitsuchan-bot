"""A cog containing a command that generates a meme out of someone."""

import urllib.parse

import discord
from discord.ext import commands
from kitsuchan.helpers import get_last_image

BASE_URL_MEMEGEN = "https://memegen.link/custom/{0}/{1}.jpg?alt={2}"


def generate_query_url(top_line, bottom_line, image_url):
    """Given an image URL, a top text line, and a bottom text line, return a memegen URL."""
    url = BASE_URL_MEMEGEN.format(urllib.parse.quote(top_line), urllib.parse.quote(bottom_line),
                                  image_url)
    return url


class Memes(commands.Cog):
    """A cog that contains a meme generator."""

    def __init__(self):
        self.footer_text = "Powered by memegen.link"

    @commands.command()
    @commands.cooldown(6, 12)
    async def meme(self, ctx, *, pair_of_lines: str):
        """Generates a meme of an image with a top line and a bottom line.

        If you attach an image when you issue the command, then the bot will use that image.
        Otherwise, it attempts to use the last image it can find in the last 100 chat messages.

        This secondary behavior is useful because you can immediately follow up a previous image
        command with this command. For example, following up an avatar command to generate a
        meme of someone's avatar.

        Example usage:

        kit meme I am | a meme
        """
        image_url = await get_last_image(ctx.channel)

        lines = pair_of_lines.split("|")

        if len(lines) < 2:
            await ctx.send("Please separate the top and bottom lines with a `|`")
            return

        top_line = lines[0].strip().replace(" ", "_")
        bottom_line = lines[1].strip().replace(" ", "_")

        url = generate_query_url(top_line, bottom_line, image_url)

        embed = discord.Embed(title="Image link")
        embed.url = url
        embed.set_image(url=url)
        embed.set_footer(text=self.footer_text)

        await ctx.send(embed=embed)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Memes())
