"""Trivia module with a trivia command."""

# pylint: disable=C0103

import html
import random

import async_timeout
import discord
from discord.ext import commands
from kitsuchan.exceptions import WebAPIInvalidResponse, WebAPIUnreachable
from kitsuchan.helpers import input_number

URL_TRIVIA_API = "https://opentdb.com/api.php?amount=1"

systemrandom = random.SystemRandom()


async def query(session, url):
    """Query OpenTDB and return the response content."""
    try:
        async with async_timeout.timeout(10):
            async with session.get(url) as response:
                if response.status == 200:
                    try:
                        response_content = await response.json()
                        return response_content
                    except Exception:
                        raise WebAPIInvalidResponse(service="opentdb.com")
                raise WebAPIUnreachable(service="opentdb.com")
    except Exception:
        raise WebAPIUnreachable(service="opentdb.com")


class Trivia(commands.Cog):
    """Trivia command."""

    @commands.command()
    @commands.cooldown(6, 12, commands.BucketType.channel)
    async def trivia(self, ctx):
        """Ask a random trivia question."""
        response_content = await query(ctx.bot.session, URL_TRIVIA_API)

        trivia = response_content["results"][0]

        correct_answer = html.unescape(trivia["correct_answer"])
        incorrect_answers = []
        for answer in trivia["incorrect_answers"]:
            incorrect_answers.append(html.unescape(answer))

        choices = [correct_answer] + incorrect_answers

        systemrandom.shuffle(choices)

        embed = discord.Embed()
        embed.title = html.unescape(trivia["category"])
        embed.description = html.unescape(trivia["question"])

        difficulty = html.unescape(trivia["difficulty"]).capitalize()
        footer_text = f"Powered by Open Trivia DB | Difficulty: {difficulty}"

        embed.set_footer(text=footer_text)

        paginator = commands.Paginator(prefix="```markdown")

        for index, value in enumerate(choices):
            paginator.add_line(f"{index+1}. {value}")

        embed.add_field(name="Options", value=paginator.pages[0])

        await ctx.send(ctx.author.mention, embed=embed)
        choice = await input_number(ctx, "Answer by number in 15 seconds.",
                                    timeout=15, min_value=1,
                                    max_value=len(choices))

        if choices[choice-1] == correct_answer:
            await ctx.send("Correct! :3")

        else:
            await ctx.send(f"Nope, the correct answer is {correct_answer}. :<")


def setup(bot):
    """Set up the extension."""
    bot.add_cog(Trivia())
