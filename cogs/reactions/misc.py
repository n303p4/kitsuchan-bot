"""Lookup commands that query various image APIs."""

# pylint: disable=C0103

import json
import secrets

import async_timeout
from discord.ext import commands
from kitsuchan.exceptions import WebAPIUnreachable

URL_RANDOM_DOG_API = "https://random.dog/woof.json"
URL_RANDOM_CAT_API = "https://nekos.life/api/v2/img/meow"
URL_RANDOM_NEKO_API = "https://nekos.life/api/neko"
URL_BIRBS_SUBREDDIT_TOP_API = "https://www.reddit.com/r/Birbs/top/.json"
URL_BIRBS_SUBREDDIT_NEW_API = "https://www.reddit.com/r/Birbs/top/.json"
URL_FOX_SUBREDDIT_TOP_API = "https://www.reddit.com/r/foxes/top/.json"
URL_FOX_SUBREDDIT_NEW_API = "https://www.reddit.com/r/foxes/new/.json"
URL_WOLF_SUBREDDIT_TOP_API = "https://www.reddit.com/r/wolves/top/.json"
URL_WOLF_SUBREDDIT_NEW_API = "https://www.reddit.com/r/wolves/new/.json"
URL_BEAVER_SUBREDDIT_TOP_API = "https://www.reddit.com/r/Beavers/top/.json"
URL_BEAVER_SUBREDDIT_NEW_API = "https://www.reddit.com/r/Beavers/new/.json"
URL_RACCOON_SUBREDDIT_TOP_API = "https://www.reddit.com/r/Raccoons/top/.json"
URL_RACCOON_SUBREDDIT_NEW_API = "https://www.reddit.com/r/Raccoons/new/.json"

async def query(session, url, service_name):
    """Given a ClientSession, URL, and service name, query an API."""
    try:
        async with async_timeout.timeout(10):
            async with session.get(url) as response:
                if response.status == 200:
                    response_content = await response.text()
                    return response_content
                else:
                    raise WebAPIUnreachable(service=service_name)
    except Exception:
        raise WebAPIUnreachable(service=service_name)


class RandomFun(commands.Cog):
    """Commands that produce random outputs."""

    @commands.command(aliases=["doge"])
    @commands.cooldown(6, 12)
    async def dog(self, ctx):
        """Fetch a random dog."""
        response_content = await query(ctx.bot.session, URL_RANDOM_DOG_API, "random.dog")
        response_content = json.loads(response_content)
        url = response_content["url"]
        await ctx.send(url)

    @commands.command(aliases=["feline"])
    @commands.cooldown(6, 12)
    async def cat(self, ctx):
        """Fetch a random cat."""
        response_content = await query(ctx.bot.session, URL_RANDOM_CAT_API, "nekos.life")
        response_content = json.loads(response_content)
        url = response_content["url"]
        await ctx.send(url)

    @commands.command(aliases=["kemonomimi", "kmimi"])
    @commands.cooldown(6, 12)
    async def kemono(self, ctx):
        """Fetch a random animal-eared person."""
        response_content = await query(ctx.bot.session, URL_RANDOM_NEKO_API, "nekos.life")
        response_content = json.loads(response_content)
        url = response_content["neko"]
        await ctx.send(url)

    async def _get_reddit_image(self, ctx, *urls):
        base_url = secrets.choice(urls)
        response_content = await query(ctx.bot.session, base_url, "Reddit")
        response_content = json.loads(response_content)
        children = response_content["data"]["children"]
        child = secrets.choice(children)
        url = child["data"]["url"]
        await ctx.send(url)

    @commands.command()
    @commands.cooldown(6, 12)
    async def birb(self, ctx):
        """Fetch a random birb."""
        await self._get_reddit_image(ctx, URL_BIRBS_SUBREDDIT_TOP_API, URL_BIRBS_SUBREDDIT_NEW_API)

    @commands.command()
    @commands.cooldown(6, 12)
    async def fox(self, ctx):
        """Fetch a random fox."""
        await self._get_reddit_image(ctx, URL_FOX_SUBREDDIT_TOP_API, URL_FOX_SUBREDDIT_NEW_API)

    @commands.command()
    @commands.cooldown(6, 12)
    async def wolf(self, ctx):
        """Fetch a random wolf."""
        await self._get_reddit_image(ctx, URL_WOLF_SUBREDDIT_TOP_API, URL_WOLF_SUBREDDIT_NEW_API)

    @commands.command()
    @commands.cooldown(6, 12)
    async def beaver(self, ctx):
        """Fetch a random beaver."""
        await self._get_reddit_image(ctx, URL_BEAVER_SUBREDDIT_TOP_API, URL_BEAVER_SUBREDDIT_NEW_API)

    @commands.command(hidden=True)
    @commands.cooldown(6, 12)
    async def raccoon(self, ctx):
        """Fetch a random raccoon."""
        await self._get_reddit_image(ctx, URL_RACCOON_SUBREDDIT_TOP_API, URL_RACCOON_SUBREDDIT_NEW_API)

def setup(bot):
    """Set up the extension."""
    bot.add_cog(RandomFun())
