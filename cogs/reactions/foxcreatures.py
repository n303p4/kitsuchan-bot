"""Commands that produce fox creatures."""

import random

from discord.ext import commands

FOXWHALE = R"""```
 /\_/\{0}   ____
/     {1}\_/  / \
| . . {1}     \  \
|  ^  {1} _   /  /
\_____{0}/ \__\_/
```"""
FOXSQUID = R"""```
  /\_/\
 /     \
 | . . |
 \  ^  /{0}
  |   |
 /// \\\
/||| |||\
|||| ||||
|       |
O       O
```"""
FOXASCII = R"""```
  /\_/\
 / . . \       ____
/__ ^ __\     /   /
  /__ \      /\/\/
  |  | \    /   /
  |  |  \__/   /
  \__/_/_\____/
```"""
FLUFFSAW = R"""```
 /\_/\{0}_____
| ˙-˙ {1}\:\_\\
 ˇˇˇˇˇ{2}ˇˇ
```"""
FOXINVADER = [
    R" /\_/\  ",
    R"/_˙-˙_\ ",
    R"[/   \] "
]
FOXJELLYFISH = [
    R" /\_/\  ",
    R"| ˙-˙ | ",
    R"/|||||\ "
]
FOXWHALE_EAT = R"""```
        /\_/\__________   ____
       / . .           \_/  / \
       ''''''\              \  \
:<>:<  ,,,,,,/          _   /  /
       \_______________/ \__\_/
```"""
FOXBEAVER = R"""```
 /\_/\
/ . . \     ____
|  ^   \   /XXX/
|       \_/XXX/
\_______/XXXX/
```"""


class FoxCreatures(commands.Cog):
    """Fox creatures cog."""

    @commands.command(aliases=["ftp"])
    @commands.cooldown(4, 1)
    async def foxtotempole(self, ctx):
        """:<:::3<:::>:3><:><><<:3:><>:3:<"""
        fox_totem_pole = ":" + random.choice(["<", "3", ">"]) + "".join(
            random.choices(["<", ":", ">", ":3"], k=random.randint(10, 30))
        )
        await ctx.send(fox_totem_pole)

    @commands.command(aliases=["fw"])
    @commands.cooldown(4, 1)
    async def foxwhale(self, ctx, length: int = 10):
        """
        The foxwhale, a creature of the ocean.

        You may specify a length - max 20, min 0.
        """
        if length not in range(0, 21):
            raise commands.UserInputError("Length must be from 0 to 20.")
        edge = "_" * length
        spaces = " " * length
        await ctx.send(FOXWHALE.format(edge, spaces))

    @commands.command(aliases=["ff"])
    @commands.cooldown(4, 1)
    async def foxfish(self, ctx, length: int = 1):
        """
        Generate a foxfish.

        You may specify a length - max 20, min 0.
        """
        if length not in range(0, 21):
            raise commands.UserInputError("Length must be from 0 to 20.")
        await ctx.send(f":<{'='*length}><")

    @commands.command(aliases=["fwe"])
    @commands.cooldown(4, 1)
    async def foxwhaleeat(self, ctx):
        """The foxwhale, eating a foxfish."""
        await ctx.send(FOXWHALE_EAT)

    @commands.command(aliases=["fs"])
    @commands.cooldown(4, 1)
    async def foxsquid(self, ctx, height: int = 1):
        """
        The foxsquid, archenemy of the foxwhale.

        You may specify a height - max 10, min 0.
        """
        if height not in range(0, 11):
            raise commands.UserInputError("Height must be from 0 to 10.")
        neck = "\n  |   |" * height
        await ctx.send(FOXSQUID.format(neck))

    @commands.command(aliases=["fa"])
    @commands.cooldown(4, 1)
    async def foxascii(self, ctx):
        """An ASCII fox."""
        await ctx.send(FOXASCII)

    @commands.command(aliases=["fb"])
    @commands.cooldown(4, 1)
    async def foxbeaver(self, ctx):
        """A foxbeaver."""
        await ctx.send(FOXBEAVER)

    @commands.command(aliases=["foxsaw", "fsaw"])
    @commands.cooldown(4, 1)
    async def fluffsaw(self, ctx, length: int = 12):
        """
        Generate a fluffsaw, an elegant weapon for a more fluffy age.

        You may specify a length - max 20, min 0.
        """
        if length not in range(0, 21):
            raise commands.UserInputError("Length must be from 0 to 20.")
        await ctx.send(FLUFFSAW.format("_"*length, " "*length, "ˇ"*length))

    async def _foxgrid(self, ctx, rows, columns, creature_segments):
        """Generic function that generates a grid of creatures."""
        if rows not in range(1, 6):
            raise commands.UserInputError("Rows must be from 1 to 5.")
        if columns not in range(1, 7):
            raise commands.UserInputError("Columns must be from 1 to 6.")
        invaders = ["```"]
        for index in range(rows):
            for segment in creature_segments:
                invaders.append(segment*columns)
            if index != rows-1:
                invaders.append("")
        invaders.append("```")
        await ctx.send("\n".join(invaders))

    @commands.command(aliases=["foxinvader", "fi"])
    @commands.cooldown(4, 1)
    async def foxinvaders(self, ctx, rows: int = 3, columns: int = 5):
        """
        Fox invaders from space have arrived!

        You may specify a number of rows - max 5, min 1.
        You may specify a number of columns - max 6, min 1.
        """
        await self._foxgrid(ctx, rows, columns, FOXINVADER)

    @commands.command(aliases=["fj"])
    @commands.cooldown(4, 1)
    async def foxjellyfish(self, ctx, rows: int = 1, columns: int = 1):
        """
        The foxjellyfish, squishy fluffy animals that live in the sea.

        You may specify a number of rows - max 5, min 1.
        You may specify a number of columns - max 6, min 1.
        """
        await self._foxgrid(ctx, rows, columns, FOXJELLYFISH)


def setup(bot):
    """Set up the extension."""
    bot.add_cog(FoxCreatures())
