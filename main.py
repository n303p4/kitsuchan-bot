#!/usr/bin/env python3
# pylint: disable=C0103

"""A minimal Discord bot made using discord.ext.commands.

Requires Python 3.6+ and discord.py (1.7+).
"""

import os
import json
import logging
import sys

import discord
from discord.ext import commands
import kitsuchan
from kitsuchan import core

assert (os.geteuid() > 0), "Please don't run me as root. :<"
assert (sys.version_info >= (3, 6)), "I require Python 3.6 or higher. :3"
assert (discord.version_info >= (1, 7)), "I require discord.py 1.7 or higher. :3"

FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)

DIRECTORY_PATH = os.path.dirname(os.path.realpath(__file__))

intents = discord.Intents.default()
intents.members = True
bot = core.Bot(command_prefix="", case_insensitive=True, pm_help=False, intents=intents, config_file="config.json")


if __name__ == "__main__":

    if os.getcwd() != DIRECTORY_PATH and "-d" in sys.argv:
        os.chdir(DIRECTORY_PATH)

    try:
        bot.load_config()
    except json.decoder.JSONDecodeError:
        print("There seems to be an error in your config.json. Kitsuchan will now halt. :<")
        sys.exit()

    assert (isinstance(bot.config.get("discord_token"), str)), "Bot token not valid."
    assert (isinstance(bot.config.get("module_blocklist", []), list)), "Module blocklist must be a list."

    bot.description = bot.config.get("description", kitsuchan.description)

    prefix = bot.config.get("prefix", "kit")
    prefixes = [
        f"{prefix} ",
        f"{prefix} ".capitalize(),
        prefix,
        prefix.capitalize()
    ]
    bot.command_prefix = commands.when_mentioned_or(*prefixes)

    blocklist = bot.config.get("module_blocklist", [])

    # Automatically load all modules.
    for dirpath, dirnames, filenames in os.walk("cogs"):
        for filename in filenames:
            if filename.endswith(".py"):
                fullpath = os.path.join(dirpath, filename).split(os.sep)
                module = ".".join(fullpath)[:-3]  # Eliminate the .py
                if module in blocklist:  # Skip blocklisted modules.
                    continue
                try:
                    bot.load_extension(module)
                except Exception as error:
                    logger.error("Unable to load %s: %s", module, error)

    bot.run(bot.config["discord_token"])
