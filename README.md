**This bot is now deprecated; see https://gitlab.com/n303p4/sailor-fox for future development**

![kitsuchan](logo.png)

[![MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/n303p4/kitsuchan-2/blob/master/LICENSE.txt)
[![Python](https://img.shields.io/badge/Python-3.6-brightgreen.svg)](https://python.org/)

**kitsuchan-bot** is a small, modular Discord bot written in `discord.py`, with a focus on
readable, portable, and easy to maintain code. It is the stock bot for the eponymous `kitsuchan`
bot framework.

# Before you begin

kitsuchan-bot has a number of modules (cogs) that are likely **not** suitable for your server.
You should review them and disable what you don't need or want.

To disable a module, run `unload <module name>`. For example, if you want to disable `exec.py`
in `cogs/owner`, you run `unload cogs.owner.exec`. To enable a module, use `load <module name>`.

You can also manually edit the configuration if you prefer. Disabled modules are listed in
`module_blocklist`.

# How to run kitsuchan-bot

kitsuchan-bot requires Python 3.6 or higher, as well as `discord.py` `rewrite`. If you're on
Windows or macOS, you should download and install Python from the
[official website](http://python.org/). If you're on Linux and your distribution ships Python
3.6 in the repositories, you should install it from there instead. Most Linux distributions
currently ship Python 3.6 or higher.

The bot should include a sample configuration file called `config.example.json`. Rename it to
`config.json` and fill it out accordingly. You will need a token to run the bot. Visit
the [Discord Developer Portal](https://discord.com/developers/applications) to create an
application, and when you create your application, click the "Bot" tab and click "Add Bot".

Once `config.json` is filled out, in a terminal run:

```bash
python3 -m pip install --user -r requirements.txt
python3 main.py
```

You may have to change the above commands slightly, depending on your operating system and the
location of your Python installation.

# Q&A

## Can I use kitsuchan-bot for my project?

Sure! Just read over [`LICENSE.txt`](LICENSE.txt) for details - it's very short, I promise! I
don't like long licenses, and you probably don't, either.

## Can I request a feature?

I don't take requests, but I do try and listen to feedback! Feel free to submit any suggestions
you have under the issue tracker. I'll be sure to ask you questions if I have any, and if I reject
your ideas, I'll try to explain my reasons as best as I can.

## I want to help! How can I contribute?

See [`CONTRIBUTING.md`](CONTRIBUTING.md) for details.
